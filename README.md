# React Bootcamp (React-native)

## Инструкции по запуску:
- Скачать или склонировать репозитори
- Установить зависимости при помощи yarn - `yarn install`
- Запустить проект - `yarn android`
- Запустить сборку build - `yarn build:android`

## Дополнительно:
### Основные сложности:
* Проект протестирован исключительно на android (Redmi Note 8 Pro)
* Проверить на планшетах возможности не было
* В режиме разработки datepicker работал адекватно(скрины ниже), однако после билда datepicker всегда выдаёт 1января 1970 год.

## Скриншоты:
![Превью](https://i.ibb.co/zskgqrS/1.jpg)
![Превью](https://i.ibb.co/DGHk9xJ/2.jpg)
![Превью](https://i.ibb.co/dryg8j8/3.jpg)
![Превью](https://i.ibb.co/VS7Zh60/4.jpg)

