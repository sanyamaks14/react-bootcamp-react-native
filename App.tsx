import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Dimensions,
    ScrollView,
} from 'react-native'
import Header from './components/main/Header/Header'
import Main from './components/main/Main/Main'
import Footer from './components/main/Footer/Footer'
import { Provider } from 'react-redux'
import store from './store/store'
// eslint-disable-next-line @typescript-eslint/no-var-requires
const imageTop = require('./assets/image/Weather-forecast-Bg-top.png')
const imageBottom = require('./assets/image/Weather-forecast-Bg-bottom.png')
const imagePart = require('./assets/image/Weather-forecast-Bg-part.png')

const { width: viewportWidth } = Dimensions.get('window')

const mobileDeviceWidth = 768
const imageTopWidth = viewportWidth * 0.6
const imageTopHeight =
    viewportWidth < mobileDeviceWidth
        ? viewportWidth * 1.2
        : viewportWidth * 0.7

const imageBottomWidth =
    viewportWidth < mobileDeviceWidth ? viewportWidth : viewportWidth * 0.7
const imageBottomHeight =
    viewportWidth < mobileDeviceWidth
        ? viewportWidth * 1.2
        : viewportWidth * 0.7

const imagePartWidth =
    viewportWidth < mobileDeviceWidth
        ? viewportWidth * 0.4
        : viewportWidth * 0.5
const imagePartHeight =
    viewportWidth < mobileDeviceWidth
        ? viewportWidth * 0.18
        : viewportWidth * 0.18

const styles = StyleSheet.create({
    app: {
        flex: 1,
        backgroundColor: '#373af5',
        width: '100%',
        height: '100%',
        minWidth: '100%',
        minHeight: '100%',
    },
    imageTop: {
        flex: 1,
        position: 'absolute',
        right: 0,
        width: imageTopWidth,
        height: imageTopHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageBottom: {
        flex: 1,
        position: 'absolute',
        bottom: 0,
        width: imageBottomWidth,
        height: imageBottomHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imagePart: {
        flex: 1,
        position: 'absolute',
        width: imagePartWidth,
        justifyContent: 'center',
        alignItems: 'center',
    },
})

export default function App(): JSX.Element {
    return (
        <Provider store={store}>
            <ScrollView style={styles.app}>
                <ImageBackground
                    source={imageTop}
                    style={styles.imageTop}
                    resizeMode="stretch"
                ></ImageBackground>
                <ImageBackground
                    source={imageBottom}
                    style={styles.imageBottom}
                    resizeMode="stretch"
                ></ImageBackground>
                <ImageBackground
                    source={imagePart}
                    style={styles.imagePart}
                    resizeMode="stretch"
                ></ImageBackground>

                <Header />
                <Main />
                <Footer />
            </ScrollView>
        </Provider>
    )
}
