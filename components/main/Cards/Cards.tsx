import React from 'react'
// eslint-disable-next-line max-len
// import { ReactComponent as IconLeft } from '../../../assets/icons/chevron-left.svg'
// eslint-disable-next-line max-len
// import { ReactComponent as IconRight } from '../../../assets/icons/chevron-right.svg'
import Card from '../Card/Card'
import { Weather7DaysCardsType } from '../../../types/types'
import { StyleSheet, View, ScrollView } from 'react-native'

type Props = {
    className?: string
    numberCards: number
    cards: Weather7DaysCardsType
}

const styles = StyleSheet.create({
    cardsContainer: {
        marginTop: 54,
        marginLeft: 24,
        flex: 1,
        alignItems: 'center',
    },
    cards: {
        flex: 1,
        flexDirection: 'row',
    },
})
const Cards = (props: Props): JSX.Element => {
    const { numberCards, cards } = props

    return (
        <View style={styles.cardsContainer}>
            <ScrollView horizontal={true} style={styles.cards}>
                {cards.map((card) => (
                    <Card
                        style={{ width: numberCards === 1 ? 300 : 'auto' }}
                        key={card?.date}
                        card={card}
                    />
                ))}
            </ScrollView>
        </View>
    )
}

export default React.memo(Cards)
