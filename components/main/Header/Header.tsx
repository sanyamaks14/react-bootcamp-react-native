import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const styles = StyleSheet.create({
    header: {
        flex: 1,
        alignItems: 'center',
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 40,
        marginBottom: 20,
        width: 320,
    },
    text: {
        flex: 1,
        color: '#fff',
        fontSize: 42,
        lineHeight: 42,
        fontWeight: 'bold',
    },
    textRight: {
        flex: 1,
        color: '#fff',
        fontSize: 48,
        lineHeight: 48,
        fontWeight: 'bold',
        textAlign: 'right',
    },
})

const Header = (): JSX.Element => {
    return (
        <View style={styles.header}>
            <View style={styles.container}>
                <Text style={styles.text}>Weather</Text>
                <Text style={styles.textRight}>forecast</Text>
            </View>
        </View>
    )
}

export default React.memo(Header)
