import React from 'react'
// eslint-disable-next-line max-len
import { ErrorType, isLoadingType } from '../../../types/types'
import { StyleSheet, View, Text } from 'react-native'
import Icon from '../../../assets/icons/Academy-Weather-bg160.svg'

type Props = {
    errorMessage: ErrorType
    isLoad: isLoadingType
}

const styles = StyleSheet.create({
    notFoundCards: {
        marginTop: 64,
        flex: 1,
        alignItems: 'center',
    },
    icon: {
        height: 40,
        width: 40,
    },
    errorMessage: {
        width: '90%',
        marginTop: 24,
        color: '#8083a4',
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'center',
    },
})

const NotFoundCards = (props: Props): JSX.Element => {
    const { errorMessage } = props

    return (
        <View style={styles.notFoundCards}>
            <Icon height={160} width={160} style={styles.icon} />
            <Text style={styles.errorMessage}>
                {errorMessage ||
                    'Fill in all the fields and the weather will be displayed'}
            </Text>
        </View>
    )
}

export default React.memo(NotFoundCards)
