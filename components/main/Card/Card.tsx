import React, { useCallback, useEffect, useState } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { WeatherCardType } from '../../../types/types'

type Props = {
    style?: {
        width: string | number
    }
    card: WeatherCardType
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        alignItems: 'center',
        height: 238,
        width: 174,
        backgroundColor: '#373af5',
        padding: 20,
        borderRadius: 8,
        shadowColor: 'rgba(5, 6, 114, 0.2)',
        shadowOpacity: 1,
        shadowRadius: 20,
        shadowOffset: { width: 14, height: 14 },
        elevation: 20,
        marginBottom: 40,
        marginRight: 20,
    },
    date: {
        fontSize: 16,
        lineHeight: 24,
        fontWeight: 'bold',
        color: '#fff',
        width: '100%',
    },
    image: {
        height: 134,
        width: 134,
    },
    temperature: {
        fontSize: 32,
        lineHeight: 32,
        fontWeight: 'bold',
        color: '#fff',
        width: '100%',
        textAlign: 'right',
    },
})

const Card = (props: Props): JSX.Element => {
    const { card, style } = props
    const [temperature, setTemperature] = useState('')
    const [date, setDate] = useState('')

    const month = useCallback(
        (index: number): string =>
            [
                'jan',
                'feb',
                'mar',
                ' apr',
                'may',
                'jun',
                'jul',
                'aug',
                'sep',
                'oct',
                'nov',
                'dec',
            ][index],
        []
    )

    useEffect(() => {
        if (card) {
            if (!card.temperature) {
                setTemperature('')
            } else {
                const temp = Math.round(card?.temperature)
                if (temp > 0) {
                    setTemperature(`+${temp}`)
                }
            }

            if (!card.date) {
                setDate('')
            } else {
                const currentDate = new Date(card?.date * 1000)
                const currentDay = currentDate.getDate()
                const currentMonth = month(currentDate.getMonth())
                const currentYear = currentDate.getFullYear()
                setDate(`${currentDay} ${currentMonth} ${currentYear}`)
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [card])

    return (
        <View style={[styles.card, style]}>
            <Text style={styles.date}>{date}</Text>
            <Image style={styles.image} source={{ uri: card.image }} />
            <Text style={styles.temperature}>{`${temperature}°`}</Text>
        </View>
    )
}

export default Card
