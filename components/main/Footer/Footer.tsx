import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const styles = StyleSheet.create({
    footer: {
        flex: 1,
        alignItems: 'center',
    },
    text: {
        marginTop: 60,
        color: '#fff',
        fontSize: 14,
        lineHeight: 18,
        textTransform: 'uppercase',
    },
})

const Footer = (): JSX.Element => {
    return (
        <View style={styles.footer}>
            <Text style={styles.text}>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</Text>
        </View>
    )
}

export default React.memo(Footer)
