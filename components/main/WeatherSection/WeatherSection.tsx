import React from 'react'
import CustomSelect from '../../shared/CustomSelect/CustomSelect'
import CustomDate from '../../shared/CustomDate/CustomDate'
import {
    CoordinatesType,
    isLoadingType,
    TimeType,
    Weather7DaysCardsType,
    ErrorType,
} from '../../../types/types'
import NotFoundCards from '../NotFoundCards/NotFoundCards'
import Cards from '../Cards/Cards'
import { StyleSheet, View, Text } from 'react-native'

type Props = {
    title: string
    styleWeatherSection?: {
        marginTop: number
        minHeight: number
    }
    numberCards: number
    errorMessage: ErrorType
    isLoad: isLoadingType
    cards: Weather7DaysCardsType
    sendCoordinates: (coordinates: CoordinatesType) => void
    sendDate?: (date: TimeType) => void
}

const styles = StyleSheet.create({
    weatherSection: {
        flex: 1,
        minHeight: 540,
        width: '100%',
        paddingTop: 32,
        paddingBottom: 32,
        backgroundColor: '#fff',
        borderRadius: 8,
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowRadius: 4,
        shadowOffset: { width: 0, height: 4 },
        elevation: 4,
    },
    title: {
        marginLeft: 24,
        marginRight: 24,
        fontSize: 32,
        lineHeight: 32,
        fontWeight: 'bold',
    },
})

const WeatherSection = (props: Props): JSX.Element => {
    const {
        styleWeatherSection,
        title,
        numberCards,
        errorMessage,
        isLoad,
        cards,
        sendCoordinates,
        sendDate,
    } = props

    return (
        <View style={[styles.weatherSection, styleWeatherSection]}>
            <Text style={styles.title}>{title}</Text>
            <View>
                <CustomSelect sendCoordinates={sendCoordinates} />
                {sendDate && (
                    <CustomDate
                        className="weather-section__input-date"
                        sendDate={sendDate}
                    />
                )}
            </View>
            {(errorMessage || cards.length === 0 || isLoad) && (
                <NotFoundCards errorMessage={errorMessage} isLoad={isLoad} />
            )}
            {!errorMessage && cards.length > 0 && !isLoad && (
                <Cards
                    className="weather-section__cards"
                    numberCards={numberCards}
                    cards={cards}
                />
            )}
        </View>
    )
}

export default React.memo(WeatherSection)
