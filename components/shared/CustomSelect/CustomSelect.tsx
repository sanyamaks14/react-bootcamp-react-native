/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable max-len */
import React, { useState } from 'react'
import {
    connectCustomSelect,
    ConnectPropsType,
} from '../../../containers/shared/CustomSelect'
import { CoordinatesType } from '../../../types/types'
import { StyleSheet, Text, View } from 'react-native'
import ModalDropdown from 'react-native-modal-dropdown'

type Props = {
    sendCoordinates: (coordinates: CoordinatesType) => void
}

const styles = StyleSheet.create({
    selector: {
        marginTop: 32,
        marginLeft: 24,
        marginRight: 24,
        height: 56,
        width: 232,
        flex: 1,
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: 'rgba(128, 131, 164, 0.2)',
        backgroundColor: 'rgba(128, 131, 164, 0.06)',
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 16,
        paddingRight: 16,
        borderRadius: 8,
    },
    text: {
        fontSize: 16,
        lineHeight: 24,
    },
    dropdown: {
        minHeight: 220,
        width: 232,
        borderRadius: 8,
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: 'rgba(128, 131, 164, 0.06)',
    },
    dropdownText: {
        height: 46,
        paddingLeft: 16,
        paddingRight: 6,
        fontSize: 16,
        lineHeight: 24,
    },
    dropdownTextHighlight: {
        backgroundColor: 'rgba(128, 131, 164, 0.06)',
    },
})

const CustomSelect = (props: ConnectPropsType & Props): JSX.Element => {
    const { cities, sendCoordinates } = props

    const defaultValue = 'Select city'
    const [currentValue, setCurrentValue] = useState(defaultValue)

    const selectValue = (id: number): void => {
        setCurrentValue(cities[id].name)
        sendCoordinates(cities[id].coordinates)
    }

    return (
        <ModalDropdown
            style={styles.selector}
            dropdownStyle={styles.dropdown}
            dropdownTextStyle={styles.dropdownText}
            dropdownTextHighlightStyle={styles.dropdownTextHighlight}
            options={cities.map((city) => city.name)}
            onSelect={selectValue}
            renderSeparator={() => <View></View>}
            adjustFrame={(style) => ({ ...style, left: style.left - 20 })}
        >
            <Text style={styles.text}>{currentValue}</Text>
        </ModalDropdown>
    )
}

export default connectCustomSelect(React.memo(CustomSelect))
