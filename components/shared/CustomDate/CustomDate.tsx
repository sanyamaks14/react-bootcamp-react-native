import React, { useEffect, useState } from 'react'
import { TimeType } from '../../../types/types'
import { StyleSheet } from 'react-native'
import DatePicker from 'react-native-datepicker'

type Props = {
    className?: string
    sendDate: (date: TimeType) => void
}

const styles = StyleSheet.create({
    date: {
        marginTop: 32,
        marginLeft: 24,
        marginRight: 24,
        width: 232,
        height: 56,
        flex: 1,
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: 'rgba(128, 131, 164, 0.2)',
        backgroundColor: 'rgba(128, 131, 164, 0.06)',
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 16,
        paddingRight: 16,
        borderRadius: 8,
    },
})

const CustomDate = (props: Props): JSX.Element => {
    const { sendDate } = props

    const defaultValue = 'Select date'
    const [date, setDate] = useState(null)
    const [minDate, setMinDate] = useState('')
    const [maxDate, setMaxDate] = useState('')

    useEffect(() => {
        const date1 = new Date()
        const date2 = new Date()
        setMinDate(
            new Date(date2.getTime() - 1000 * 24 * 60 * 60 * 5)
                .toLocaleString('en-GB')
                .split(',')[0]
                .split('/')
                .join('-')
        )
        setMaxDate(
            new Date(date1.getTime() - 1000 * 24 * 60 * 60)
                .toLocaleString('en-GB')
                .split(',')[0]
                .split('/')
                .join('-')
        )
    }, [])

    const getDateNow = (d): void => {
        setDate(d)
        const arrD = d.split('-')
        const newD = [arrD[1], arrD[0], arrD[2]].join('-')
        const dateNow = new Date(newD)
        sendDate(dateNow.getTime() / 1000)
    }

    return (
        <DatePicker
            style={styles.date}
            date={date}
            mode="date"
            placeholder={defaultValue}
            format="DD-MM-YYYY"
            minDate={minDate}
            maxDate={maxDate}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={{
                dateIcon: {
                    position: 'absolute',
                    left: -100,
                    marginLeft: 0,
                },
                dateInput: {
                    top: -6,
                    left: -60,
                    borderWidth: 0,
                    fontSize: 16,
                    lineHeight: 24,
                },
            }}
            onDateChange={(d) => {
                getDateNow(d)
            }}
        />
    )
}

export default React.memo(CustomDate)
